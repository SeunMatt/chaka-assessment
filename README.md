# Backend Engineer Assessment

This is the solution to Backend Engineer Assessment by Chaka.

The solution logic is contained in [ChakaAssessmentService.java](src/main/java/com/smattme/chakkaassessment/ChakaAssessmentService.java) 
while [ChakaAssessmentServiceUnitTest.java](src/test/java/com/smattme/chakkaassessment/ChakaAssessmentServiceUnitTest.java) contains the associated unit tests.

## Author
Seun Matt

## Software Requirements
- JDK 11+
- Maven 3.6+

## Running Tests
`./mvnw clean test`