package com.smattme.chakkaassessment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class ChakaAssessmentService {

    private static final Logger logger = LoggerFactory.getLogger(ChakaAssessmentService.class);


    /**
     * this function will decoded the encoded string and return
     * the letter at index k. Treating k as 1-index based
     * @param encodedString to be decoded
     * @param k - the kth index
     * @return the kth letter of the decoded string
     * @throws OutOfMemoryError when the decoded String is too large
     */
    public static String decodeAndFindKthLetter(String encodedString, int k) {

        /*
            The technique I will use here is to decode the encoded string
            as described in the assessment doc.
            I'll use StringBuilder instead of concatenating strings to
            improve efficiency.
         */

        //this is an early catch to return the first letter if it's the
        // only letter in the encodedString
        if(encodedString.substring(1).matches("[0-9]+"))
            return String.valueOf(encodedString.charAt(0));

        StringBuilder decodedStringBuffer = new StringBuilder();
        for (char character: encodedString.toCharArray()) {
            var charString = String.valueOf(character);
            if(Character.isDigit(character)) {
                int i = Integer.parseInt(charString) - 1;
                 decodedStringBuffer.append(decodedStringBuffer.toString().repeat(i));
            }
            else {
                decodedStringBuffer.append(charString);
            }
        }

        return String.valueOf(decodedStringBuffer.charAt(k - 1));
    }

    /**
     * This function combines subtract different elements of numbers from x
     * with the goal of reducing x to zero in the shortest possible step
     * @param numbers - the array of possible elements to combine to reduce x
     * @param x - the number to be reduced to zero
     * @return the number of min. operations to reduce x to zero
     */
    public static int reduceXAndReturnOpsCount(int [] numbers, int x) {

        //sort the numbers in ascending order from smallest to biggest
        Arrays.sort(numbers);

        /*
            if the first element of the array is bigger than x
            then return -1 because subtracting its value (and subsequent ones) from x will
            lead to the negative
         */
        if(numbers[0] > x) return  -1;

        /*
         * since we've sorted the array, the logic is to
         * iterate over the array starting from the last element
         * which will be the biggest element and walk backwards
         * until x == 0
         * If we encounter an element that's bigger than x, we will simply
         * skip such element.
         * For every subtraction operation, we will increase the step count by 1
         */

        int steps = 0;
        for (int i = numbers.length - 1; i >= 0; i--) {
            var number = numbers[i];
            if(number > x) continue;
            x = x - number;
            steps++;
            if (x == 0) break;
        }

        //finally, return the steps
        return steps;
    }


}
