package com.smattme.chakkaassessment;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ChakaAssessmentServiceUnitTest {

    private static final Logger logger = LoggerFactory.getLogger(ChakaAssessmentServiceUnitTest.class);

    @Test
    void givenFirstSampleStringAndIndexK_whenDecodeAndFindKthLetter_thenReturnKthLetter() {
        String s = "ha22";
        int k = 5;
        String kthLetter = ChakaAssessmentService.decodeAndFindKthLetter(s, k);
        assertEquals("h", kthLetter);
    }

    @Test
    void givenSecondStringAndIndexK_whenDecodeAndFindKthLetter_thenReturnKthLetter() {
        String s = "chaka2stocks3";
        int k = 10;
        String kthLetter = ChakaAssessmentService.decodeAndFindKthLetter(s, k);
        assertEquals("a", kthLetter);
    }

    @Test
    void givenIntegerArrayAndX_whenReduceX_thenReturnStepsCount() {
        int [] numbers = {3,2,20,1,1,3};
        int x = 10;
        int steps = ChakaAssessmentService.reduceXAndReturnOpsCount(numbers, x);
        assertEquals(5, steps);
    }


}
